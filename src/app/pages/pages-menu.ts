import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Inicio',
    icon: 'home',
    link: '/pages/dashboard',
    home: true,
  },
 
  {
    title: 'Ubicaciones',
    icon: 'layout-outline',
    link: '/pages/layout/stepper',    
  },
  {
    title: 'Usuarios',
    icon: 'layout-outline',
    link: '',    
  },
  {
    title: 'Reportes',
    icon: 'layout-outline',
    link: '',   
  },
];
