import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbListModule,
  NbRouteTabsetModule,
  NbInputModule,
  NbIconModule,
  NbRadioModule,
  NbTreeGridModule,
  NbStepperModule,
  NbSelectModule,
  NbTabsetModule, NbUserModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { Tab1Component, Tab2Component, TabsComponent } from './tabs/tabs.component';
import { StepperComponent } from './stepper/stepper.component';
import { ListComponent } from './list/list.component';
import { InfiniteListComponent } from './infinite-list/infinite-list.component';
import { NewsPostComponent } from './infinite-list/news-post/news-post.component';
import { NewsPostPlaceholderComponent } from './infinite-list/news-post-placeholder/news-post-placeholder.component';
import { AccordionComponent } from './accordion/accordion.component';
import { NewsService } from './news.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NbInputModule,
    NgSelectModule,
    NbIconModule,
    NbRadioModule,
    ThemeModule,
    NbTabsetModule,
    NbTreeGridModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    Ng2SmartTableModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbSelectModule,
    LayoutRoutingModule,
  ],
  declarations: [
    LayoutComponent,    
    
    TabsComponent,
    Tab1Component,
    Tab2Component,
    StepperComponent,
    ListComponent,
    NewsPostPlaceholderComponent,
    InfiniteListComponent,
    NewsPostComponent,
    AccordionComponent,
  ],
  providers: [
    NewsService,
    
  ],
})
export class LayoutModule { }
