import { Component,  ViewChild } from '@angular/core';
import {NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { NgSelectConfig } from '@ng-select/ng-select';


@Component({
  selector: 'ngx-stepper',
  templateUrl: 'stepper.component.html',
  styleUrls: ['stepper.component.scss'],
})

export class StepperComponent { 
  selectedCity: string;
  pisos = [
    {id: 1, name:'piso 1'}
    //{id: 2, name:'piso 2'},
    //{id: 3, name:'piso 3'},
    //{id: 4, name:'piso 4'},
    //{id: 5, name:'piso 5'},
    //{id: 6, name:'piso 6'},
    //{id: 7, name:'piso 7'},
    //{id: 8, name:'piso 8'}
];
  cities = [
    {id: 1, name: 'Chorrillos'},
    {id: 2, name: 'San Isidro'},
    {id: 3, name: 'Miraflores'},
    {id: 4, name: 'Jockey Plaza'},
    {id: 5, name: 'San Miguel'}
];
show:boolean;
show2:boolean;
show3:boolean;
show4:boolean;
  constructor(private config2: NgSelectConfig,private toastrService: NbToastrService,private service: SmartTableData) {
    this.show=false;
    this.show2=false;
    this.show3=false;
    this.show4=false;
    this.config2.notFoundText = 'Custom not found';
    
    const data = [
      {
        id: 1,
        firstName: "Leanne Graham",
        lastName: "Bret",
        username: "Sincere@april.biz",
        age: '<button type="button" class="btn btn-primary">Primary</button>'
      },
      {
        id: 2,
        firstName: "Ervin Howell",
        lastName: "Antonette",
        username: "Shanna@melissa.tv",
        age: "Sincere@april.biz"
      },
      
      // ... list of items
      
      {
        id: 11,
        firstName: "Nicholas DuBuque",
        lastName: "Nicholas.Stanton",
        username: "Rey.Padberg@rosamond.biz",
        age: "Sincere@april.biz"
      }
    ];
    this.source.load(data);
  }
  config: ToasterConfig;

  @ViewChild('item', { static: true }) accordion;

  toggle() {
    this.accordion.toggle();
  }
  index = 1;
  destroyByClick = true;
  duration = 1;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';

  title = 'HI there!';
  content = `I'm cool toaster!`;

  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  quotes = [
    { title: null, body: 'We rock at Angular' },
    { title: null, body: 'Titles are not always needed' },
    { title: null, body: 'Toastr rock!' },
  ];
AgregarPiso(){
  debugger;
  this.show4=true;
}
  validacionSucursal(){
    debugger;
    if(this.selectedCity=='1'){
      this.show=true;
      this.show2=false;
      this.show3=false;
      this.show4=false;
    }else if(this.selectedCity=='2'){
      this.show=false;
      this.show2=true;
      this.show3=false;
      this.show4=false;
    }else if(this.selectedCity=='3'){
      this.show=false;
      this.show2=false;
      this.show3=true;
      this.show4=false;
    }else if(this.selectedCity=='4'){
      this.show=false;
      this.show2=false;
      this.show3=true;
      this.show4=false;
    }else if(this.selectedCity=='5'){
      this.show=false;
      this.show2=false;
      this.show3=false;
      this.show4=false;
    }
  }
  makeToast() {
    this.showToast(this.status, this.title, this.content);
  }

  openRandomToast () {
    const typeIndex = Math.floor(Math.random() * this.types.length);
    const quoteIndex = Math.floor(Math.random() * this.quotes.length);
    const type = this.types[typeIndex];
    const quote = this.quotes[quoteIndex];

    this.showToast(type, quote.title, quote.body);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `Toast ${this.index}${titleContent}`,
      config);
  }
  settings = {   
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Piso',
        type: 'number',
      },
      firstName: {
        title: 'Codigo de Ubicación',
        type: 'string',
      },
      lastName: {
        title: 'Nombre',
        type: 'string',
      },
      username: {
        title: 'Descripcion',
        type: 'string',
      },  
      age: {
        title: 'Tipo de Ubicacion',        
        type:'costum',        
      },  
        
    },
  };

  source: LocalDataSource = new LocalDataSource();

 

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
