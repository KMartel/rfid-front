import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {AuthResponse} from '../interfaces/authResponse';

import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-pages', 
  styleUrls: ['./login.component.scss'],
  template: `
  <nb-layout class="col-12 col-md-8">   
    <nb-layout-column style="padding: 0; text-align: center;z-index: 0;">
      
      
    <particles [style]="myStyle" [params]="myParams"></particles>
    
     <div class="wrap-login100">
        <div class="centrar">
          <img class="img-fluid loginpic" src="assets/images/logo_rfid.png" alt="RFID" width="200px" height="100"/>
        </div>
        <form novalidate (ngSubmit)="addEmployee()" [formGroup]="user">
          <div class="form-group">
            <div class="wrap-input100">
              <input class="input100" type="text" placeholder="Usuario" formControlName="name" />
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-user-circle" aria-hidden="true"></i>
              </span>
            </div>

          <div class="wrap-input100">
            <input class="input100" type="password" placeholder="Contraseña"  formControlName="password"/>
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
          </div>
        
          <button class="login100-form-btn" type="submit">
            Iniciar Sesion
          </button>
        </div>
      </form>
      <div class="centrar" style="padding-top: 20px;">
        <img class="img-fluid loginpic" src="assets/images/logo_ripley_com.png" alt="Ripley.com" width="150px" height="200"/>
      </div>  
  </div>   


  
  <router-outlet></router-outlet>  
  </nb-layout-column>   
  
   </nb-layout>
  
  `,
})

export class LoginComponent {

  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  user: FormGroup;
  constructor(
    private authService:AuthService,
    public router: Router,
     private toastr: ToastrService
  ){ }
   ngOnInit() {
    this.user = new FormGroup({
      name: new FormControl(''),
      password: new FormControl('')
    });
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 0
          
  };
  this.myParams = {    
    particles: {
        number: {
            value: 70,
            density: {
              enable: true,
              value_area: 1000
            }
        },
        color: {
            value: ["#ca1221","#f7cb08","#f5f5ff","#d0d0d2","#6e215d"]
        },
        shape: {
            type: 'circle',
            stroke: {
                width: 0,
                color: "#000000"
              },
              polygon: {
                nb_sides: 5
              },
              image: {                      
                width: 100,
                height: 100,
                type:"circle"
              }
        },
        opacity: {
            value: 0.5,
            random: false,
            anim: {
              enable: false,
              speed: 1,
              opacity_min: 0.1,
              sync: false
            }
          },
          size: {
            value: 20,
            random: true,
            anim: {
              enable: false,
              speed: 40,
              size_min: 0.1,
              sync: false
            }
          },
          line_linked: {
            enable: true,
            distance: 150,
            color: "#000",
            opacity: 1,
            width: 1
          },                  
          move: {
            enable: true,
            speed: 6,
            direction: "none",
            random: false,
            straight: false,
            out_mode: "out",
            bounce: false,
            attract: {
              enable: false,
              rotateX: 600,
              rotateY: 1200
            }
          }
},
interactivity: {
detect_on: "canvas",
events: {  
  onhover: {
    enable: false,
    mode: "repulse"
  },
  onclick: {
    enable: false,
    mode: "push"
  },
 
},
modes: {
  grab: {
    distance: 400,
    line_linked: {
      opacity: 1
    }
  },
  bubble: {
    distance: 400,
    size: 40,
    duration: 2,
    opacity: 8,
    speed: 3
  },
  repulse: {
    distance: 200,
    duration: 0.4
  },
  push: {
    particles_nb: 4
  },
  remove: {
    particles_nb: 2
  }
}
}

};

};
  
  addEmployee() {
    localStorage.setItem('name', this.user.get('name').value);
    //localStorage.setItem('password', this.user.get('password').value);
    console.log(this.user.get('name').value);
    console.log(this.user.get('password').value);
    const task = {
      client_id:'apiconnect',
   client_secret:'71D58FDC-8467-4D61-9010-DCE401F16A78',
   grant_type:'password',
    username:this.user.get('name').value,    
    //'USERADM1'
    password:this.user.get('password').value,
    //'ripley123'
    scope:'securityapi'  
    };
    this.authService.createTask2(task).subscribe((data: {}) => {     
      console.info(data);
      this.router.navigate(['/pages'])
    },
    error=>{
      this.toastr.error('Usuario y Contraseña erronea','Error: ')
      console.log(error);
    })
  }
  
  createTask() {
    console.log(this.user.get('name').value);
    console.log(this.user.get('password').value);
    const task = {
      client_id:'apiconnect',
   client_secret:'71D58FDC-8467-4D61-9010-DCE401F16A78',
   grant_type:'password',
    username:this.user.get('name').value,
    //'USERADM1'
    password:this.user.get('password').value,
    //'ripley123'
    scope:'securityapi'  
    };
    
    this.authService.createTask(task)
    .subscribe((newTask:AuthResponse)  => {  
      const task2:AuthResponse =newTask;  
      console.info(task2);
    });
  }
}
