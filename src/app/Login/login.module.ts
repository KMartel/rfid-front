
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';

import { 
     NbLayoutModule,
    NbSidebarModule, // NbSidebarModule.forRoot(), //if this is your app.module
    NbButtonModule,
    NbToastrModule,
    NbMenuModule,
} from '@nebular/theme';
import { LoginComponent } from './login.component';
import { ThemeModule } from '../@theme/theme.module';
import { ECommerceModule } from '../pages/e-commerce/e-commerce.module';
import { DashboardModule } from '../pages/dashboard/dashboard.module';
import { MiscellaneousModule } from '../pages/miscellaneous/miscellaneous.module';
import { ParticlesModule } from 'angular-particle';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  imports: [
    LoginRoutingModule,
    ThemeModule,
    NbLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    NbToastrModule.forRoot(),
    NbMenuModule,
    ParticlesModule,
    NbSidebarModule,
    DashboardModule,  
    NbButtonModule,  
    ECommerceModule,
    MiscellaneousModule,  
  ],
  declarations: [
    // ... here goes our new components
    LoginComponent
  ],
})
export class LoginModule {
}