import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Auth } from './../interfaces/auth';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { AuthResponse } from '../interfaces/authResponse';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/x-www-form-urlencoded',
    'Accept': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private index: number = 0;
  constructor(
    private http: HttpClient,
    private toastr: ToastrService
  ) {
  
   }
   
  getAllTasks() {
    const path = 'https://jsonplaceholder.typicode.com/todos';
    return this.http.get<Auth[]>(path);
  }
  
  getTask(id: string) {
    const path = `https://jsonplaceholder.typicode.com/todos/${id}`;
    return this.http.get<Auth>(path);
  }

  createTask2(task): Observable<AuthResponse> {
    debugger;
    const form = 'client_id='+task.client_id+'&client_secret='+task.client_secret+'&grant_type='+task.grant_type+'&username='+task.username+'&password='+task.password+'&scope='+task.scope
    console.log(form)
    const  path = `http://QALBWRPIDENTITY-215726963.us-east-1.elb.amazonaws.com/connect/token`;
    return this.http.post<AuthResponse>(path,form, httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  createTask(task: Auth) {    
    const form = 'client_id='+task.client_id+'&client_secret='+task.client_secret+'&grant_type='+task.grant_type+'&username='+task.username+'&password='+task.password+'&scope='+task.scope
    console.log(form)
    const  path = `http://QALBWRPIDENTITY-215726963.us-east-1.elb.amazonaws.com/connect/token`;
    return this.http.post(path, form,httpOptions);
  }

  // Error handling 
  handleError(error) {
    debugger;
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     
     return throwError(errorMessage);
  }

 

}
